import 'package:flutter/material.dart';

class DialogLaunchers {
  static Future showDialog(
      {@required BuildContext context,
      @required Widget dialog,
      Color barrierColor}) {
    return showGeneralDialog(
        context: context,
        barrierColor: barrierColor,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context).alertDialogLabel,
        transitionDuration: Duration(milliseconds: 400),
        pageBuilder: (_, __, ___) {
          return dialog;
        });
  }
}
