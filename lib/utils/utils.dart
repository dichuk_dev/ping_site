import 'package:favicon/favicon.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ping_site/domain/website.dart';

var urlPattern =
    r'^((?:.|\n)*?)((http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?)';

class Utils {
  static Future<Website> getWebsiteInfo(
      {@required String url, bool isCheckIcon = true}) async {
    var hasMatch = new RegExp(urlPattern, caseSensitive: false).hasMatch(url);
    if (hasMatch) {
      try {
        final response = await http.get(Uri.parse(url));
        return Website(
            url: url,
            httpStatus: '${response.statusCode}',
            isAvailable: true,
            icon: isCheckIcon &&
                    response.statusCode >= 200 &&
                    response.statusCode <= 210
                ? Image.network((await Favicon.getBest(url)).url)
                : Container());
      } catch (e) {
        return Website(
          url: url,
          httpStatus: null,
          isAvailable: false,
        );
      }
    }
    return Website(url: url, httpStatus: null, isAvailable: false);
  }
}
