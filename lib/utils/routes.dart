import 'package:flutter/material.dart';
import 'package:ping_site/ui/screens/dashboard/dashboard.dart';
import 'package:ping_site/ui/screens/splash/splash.dart';

class Routes {
  Routes._();
  static const String dashboard = '/dashboard';
  static const String splash = '/splash';

  static final routes = <String, WidgetBuilder>{
    dashboard: (BuildContext context) => DashboardScreen(),
    splash: (BuildContext context) => SplashScreen(),
  };
}
