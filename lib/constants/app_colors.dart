import 'dart:ui';

import 'package:flutter/material.dart';

const kBase = Color(0xff1F2023);
const kOverflow = Color(0xff010101);
const kLighter = Color(0xffd5d5d6);
const kOverflowDark = Color(0xff2d2f34);
const kSurface = Color(0xff27292d);
const kGrey1 = Color(0xffc2d1d9);
const kGrey = Color(0xff757575);
const kPink = Color(0xffff3062);
const kYellow = Color(0xffFFD300);
const kAppGreen = Color(0xff0CC886);
const kYellowColor = Colors.yellow;
const kAppPurpleColor = Color(0xffFF4848);
const kWhite = Colors.white;
