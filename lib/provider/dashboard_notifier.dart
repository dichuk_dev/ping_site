import 'package:flutter/material.dart';
import 'package:ping_site/domain/website.dart';
import 'package:ping_site/provider/core/base_notifier.dart';
import 'package:ping_site/provider/core/view_state.dart';
import 'package:ping_site/utils/utils.dart';
import 'package:rxdart/rxdart.dart';

class DashboardNotifier extends BaseNotifier {
  var webSiteSubject = BehaviorSubject<List<Website>>();

  Stream<List<Website>> get webSiteStream => webSiteSubject.stream;

  void close() {
    webSiteSubject.close();
  }

  Future deleteItem({@required Website item}) async {
    setState(ViewState.Busy);
    var temp = webSiteSubject.value;
    temp.remove(item);
    webSiteSubject.sink.add(temp);
    setState(ViewState.Idle);
  }

  Future addItem({@required String url}) async {
    if (url == null) return;
    setState(ViewState.Busy);
    Website item = await Utils.getWebsiteInfo(url: url);
    if (item == null) return;
    List<Website> tempDataList = [];
    if (webSiteSubject != null &&
        webSiteSubject.valueOrNull != null &&
        webSiteSubject.valueOrNull.length > 0) {
      webSiteSubject.value.forEach((element) {
        tempDataList.add(element);
      });
    }
    tempDataList.add(item);
    webSiteSubject.sink.add(tempDataList);
    setState(ViewState.Idle);
  }

  Future<bool> refresh() async {
    bool isRefresh = false;
    setState(ViewState.Busy);
    if (webSiteSubject != null &&
        webSiteSubject.valueOrNull != null &&
        webSiteSubject.valueOrNull.length > 0) {
      List<Website> tempDataList = [];
      await Future.forEach(webSiteSubject.value, (item) async {
        Website website =
            await Utils.getWebsiteInfo(url: item.url, isCheckIcon: false);
        website.icon = item.icon;
        tempDataList.add(website);
      });
      isRefresh = true;
      webSiteSubject.sink.add(tempDataList);
    }
    setState(ViewState.Idle);
    return isRefresh;
  }
}
