import 'package:flutter/material.dart';
import 'package:ping_site/provider/core/view_state.dart';

class BaseNotifier with ChangeNotifier {
  ViewState _state = ViewState.Idle;

  ViewState get state => _state;
  bool _disposed = false;

  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }

  @override
  void dispose() {
    _disposed = true;
    super.dispose();
  }

  @override
  void notifyListeners() {
    if (!_disposed) {
      super.notifyListeners();
    }
  }
}
