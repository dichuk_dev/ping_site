import 'package:flutter/material.dart';

class MyCalulateArea extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyCalculateAreaState();
}

class MyCalculateAreaState extends State {
  final _formKey = GlobalKey<FormState>();
  var _numberOne = 0;
  var _numberTwo = 0;
  var _area = 0;

  Widget build(BuildContext context) {
    return new Form(
        key: _formKey,
        child: new Column(children: [
          new Row(children: <Widget>[
            new Container(
                padding: EdgeInsets.all(10.0), child: new Text('Перше число')),
            new Expanded(
                child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: new TextFormField(validator: (value) {
                      if (value.isEmpty) return "Вкажіть число";
                      try {
                        _numberOne = int.parse(value);
                      } catch (e) {
                        _numberOne = 0;
                        return e.toString();
                      }
                    }))),
          ]),
          new SizedBox(height: 10.0),
          new Row(children: <Widget>[
            new Container(
                padding: EdgeInsets.all(10.0), child: new Text('Друге число')),
            new Expanded(
                child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: new TextFormField(validator: (value) {
                      if (value.isEmpty) return "Вкажіть число";
                      try {
                        _numberTwo = int.parse(value);
                      } catch (e) {
                        _numberTwo = 0;
                        return e.toString();
                      }
                    }))),
          ]),
          new SizedBox(height: 10.0),
          new RaisedButton(
            onPressed: () {
              print('onPressed');
              setState(() {
                if (_numberOne is int && _numberTwo is int)
                  _area = _numberOne + _numberTwo;
              });
            },
            child: Text('Вирахувати'),
            color: Colors.blue,
            textColor: Colors.white,
          ),
          new SizedBox(height: 50.0),
          new Text(
            _area == null ? "Задайте параметри" : "",
            style: TextStyle(fontSize: 30.0),
          )
        ]));
  }
}

void main() => runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new Scaffold(
        appBar: new AppBar(title: new Text('Калькулятор')),
        body: new MyCalulateArea())));
