import 'package:flutter/material.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/domain/website.dart';
import 'package:ping_site/ui/styles/custom_text_style.dart';

class ItemList extends StatefulWidget {
  final Website item;

  const ItemList({Key key, this.item}) : super(key: key);

  @override
  _ItemListState createState() => _ItemListState();
}

class _ItemListState extends State<ItemList> {
  @override
  Widget build(BuildContext context) {
    Website item = widget.item;

    return Container(
      padding: EdgeInsets.all(19),
      color: kOverflowDark,
      child: Row(
        children: [
          Container(width: 26, height: 26, child: item.realIcon),
          Padding(
              padding: EdgeInsets.only(left: 19, right: 9),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.url,
                    style: CustomTextStyle().set(FontWeight.w600, 19, kWhite),
                  ),
                  Row(
                    children: [
                      Text(
                        'Http Code: ${item.status}',
                        style: CustomTextStyle().set(
                            FontWeight.w600, 14, getStatusColor(item.status)),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Last checked: ${item.updatedAt}',
                        style:
                            CustomTextStyle().set(FontWeight.w600, 12, kWhite),
                      ),
                    ],
                  )
                ],
              ))
        ],
      ),
    );
  }

  Color getStatusColor(String statusCode) {
    Color color = kAppGreen;
    if (isNumeric(statusCode)) {
      int number = int.parse(statusCode);
      if (number >= 200 && number < 210)
        color = kAppGreen;
      else
        color = kPink;
    } else {
      color = kYellowColor;
    }
    return color;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }
}
