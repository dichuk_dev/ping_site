import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/domain/website.dart';
import 'package:ping_site/provider/dashboard_notifier.dart';
import 'package:ping_site/ui/dialogs/add_site_dialog.dart';
import 'package:ping_site/ui/screens/dashboard/widgets/item_list.dart';
import 'package:ping_site/ui/widgets/custom_toobar.dart';
import 'package:ping_site/ui/widgets/loading_widget.dart';
import 'package:ping_site/utils/dialog_launchers.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomToolbar(),
        backgroundColor: kOverflow,
        body: Consumer<DashboardNotifier>(builder: (context, model, child) {
          return Container(
              padding: EdgeInsets.only(top: 24, bottom: 24),
              child: Stack(
                children: [
                  Flex(direction: Axis.vertical, children: [
                    Expanded(
                        child: StreamBuilder<List<Website>>(
                            stream: model.webSiteStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return ListView.builder(
                                    shrinkWrap: true,
                                    padding: EdgeInsets.only(
                                      left: 0,
                                      right: 0,
                                    ),
                                    physics: AlwaysScrollableScrollPhysics(),
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (context, index) {
                                      Website item = snapshot.data[index];
                                      return Container(
                                          padding: EdgeInsets.only(
                                              top: 4, bottom: 4),
                                          child: Slidable(
                                            actionPane:
                                                SlidableScrollActionPane(),
                                            actionExtentRatio: 0.25,
                                            child: ItemList(item: item),
                                            actions: <Widget>[
                                              IconSlideAction(
                                                caption: 'Delete',
                                                color: kPink,
                                                icon: Icons.delete,
                                                onTap: () => model
                                                    .deleteItem(item: item)
                                                    .then((value) =>
                                                        ScaffoldMessenger.of(
                                                                context)
                                                            .showSnackBar(
                                                          SnackBar(
                                                            content: Text(
                                                              "Detele item ${item.url}",
                                                            ),
                                                            duration: Duration(
                                                                milliseconds:
                                                                    500),
                                                          ),
                                                        )),
                                              ),
                                            ],
                                          ));
                                    });
                              } else {
                                return Container();
                              }
                            }))
                  ]),
                  Loading(viewState: model.state)
                ],
              ));
        }),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            DialogLaunchers.showDialog(
                context: context, dialog: AddSiteDialog());
          },
          tooltip: 'Add Site',
          backgroundColor: kPink,
          child: Icon(Icons.add),
        ));
  }
}
