import 'package:flutter/material.dart';
import 'package:ping_site/bloc/splash_bloc.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/ui/styles/custom_text_style.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Provider.of<SplashBloc>(context, listen: false).initPage().then(
        (routes) => Future.delayed(const Duration(milliseconds: 2000), () {
              Navigator.of(context).pushReplacementNamed(routes);
            }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: kOverflow,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: height,
        child: Center(
            child: Text(
          'Websites Availability\nChecker',
          textAlign: TextAlign.center,
          style: CustomTextStyle().set(FontWeight.w500, height / 26, kPink),
        )),
      ),
    );
  }
}
