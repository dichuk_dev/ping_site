import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/provider/dashboard_notifier.dart';
import 'package:ping_site/ui/styles/custom_text_style.dart';
import 'package:provider/provider.dart';

class CustomToolbar extends StatelessWidget implements PreferredSizeWidget {
  const CustomToolbar({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return SafeArea(
        child: Container(
            color: kSurface,
            child: Stack(
              children: <Widget>[
                Positioned(
                    top: height / 36,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Text(
                      'Web-sites',
                      textAlign: TextAlign.center,
                      style: CustomTextStyle()
                          .set(FontWeight.normal, 24, Colors.white),
                    )),
                Positioned(
                  top: height / 36,
                  right: 16,
                  child: IconButton(
                      onPressed: () async {
                        context
                            .read<DashboardNotifier>()
                            .refresh()
                            .then((isRefresh) {
                          if (isRefresh)
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  "Refresh data",
                                ),
                                duration: Duration(milliseconds: 500),
                              ),
                            );
                        });
                      },
                      icon: SvgPicture.asset(
                        'assets/images/svg/ic_refresh.svg',
                        color: kPink,
                      )),
                ),
              ],
            )));
  }

  @override
  Size get preferredSize => Size.fromHeight(90.0);
}
