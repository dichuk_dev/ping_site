import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final double radius;
  final Color background;
  final Color textColor;
  final double height;
  final double width;
  final FontWeight fontWeight;
  final double fontSize;

  Button(
      {@required this.text,
      @required this.onPressed,
      this.background = Colors.teal,
      this.height = 0,
      this.textColor = Colors.white,
      this.width,
      this.radius = 12,
      this.fontWeight,
      this.fontSize});

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var oldHeight;
    if (this.height == 0) {
      oldHeight = height / 21;
    } else {
      oldHeight = this.height;
    }
    return Container(
      height: oldHeight,
      width: width == null ? MediaQuery.of(context).size.width - 40 : width,
      decoration: BoxDecoration(
          color: background,
          borderRadius: BorderRadius.all(Radius.circular(radius))),
      child: RawMaterialButton(
        onPressed: onPressed,
        elevation: 0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: fontSize != null ? fontSize : height / 47.6,
            fontStyle: FontStyle.normal,
            fontWeight: fontWeight != null ? fontWeight : FontWeight.normal,
            color: textColor,
          ),
        ),
      ),
    );
  }
}
