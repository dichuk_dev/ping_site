import 'package:flutter/material.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/ui/styles/custom_text_style.dart';

class CustomForm extends StatelessWidget {
  final TextEditingController controller;

  const CustomForm({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: height / 40),
        child: Container(
            height: height / 19,
            decoration: BoxDecoration(
              color: kLighter,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 3, bottom: 3),
              child: TextFormField(
                controller: controller,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.done,
                onFieldSubmitted: (value) {},
                maxLines: 1,
                style:
                    CustomTextStyle().set(FontWeight.normal, 17, kOverflowDark),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(
                      top: 1,
                      left: width / 44.75,
                      right: width / 44.75,
                      bottom: width / 31.75),
                  fillColor: Color.fromRGBO(118, 118, 128, 0.12),
                  focusedBorder: InputBorder.none,
                  border: InputBorder.none,
                ),
              ),
            )),
      ),
    );
  }
}
