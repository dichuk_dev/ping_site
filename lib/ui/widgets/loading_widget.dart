import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/provider/core/view_state.dart';

class Loading extends StatelessWidget {
  final ViewState viewState;

  const Loading({Key key, this.viewState}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return viewState == ViewState.Busy
        ? Positioned.fill(
            child: Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  color: kWhite,
                )))
        : Container();
  }
}
