import 'package:flutter/material.dart';

class CustomTextStyle {
  TextStyle set(FontWeight fontWeight, double fontSize, Color color,
      {double letterSpacing = -0.4,
      FontStyle fontStyle = FontStyle.normal,
      TextDecoration decoration = TextDecoration.none,
      double height = 1.5}) {
    return TextStyle(
        fontSize: fontSize,
        fontStyle: fontStyle,
        fontWeight: fontWeight,
        color: color,
        letterSpacing: letterSpacing,
        height: height,
        decoration: decoration);
  }
}
