import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ping_site/constants/app_colors.dart';
import 'package:ping_site/provider/dashboard_notifier.dart';
import 'package:ping_site/ui/styles/custom_text_style.dart';
import 'package:ping_site/ui/widgets/custom_button.dart';
import 'package:ping_site/ui/widgets/custom_form.dart';
import 'package:provider/provider.dart';

class AddSiteDialog extends StatefulWidget {
  @override
  _AddSiteDialogState createState() => _AddSiteDialogState();
}

class _AddSiteDialogState extends State<AddSiteDialog> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final urlController = TextEditingController();
  @override
  void dispose() {
    urlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Align(
        key: _scaffoldKey,
        alignment: Alignment.center,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: width / 20),
          child: Material(
            elevation: 0,
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                color: kBase,
                borderRadius: BorderRadius.all(Radius.circular(12)),
                border: Border.all(
                  color: kYellow.withOpacity(0.7),
                  width: 0.5,
                ),
              ),
              padding: EdgeInsets.all(width / 21.88),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    'Add new website',
                    style: CustomTextStyle().set(FontWeight.w600, 18, kWhite),
                  ),
                  SizedBox(
                    height: height / 46,
                  ),
                  CustomForm(
                    controller: urlController,
                  ),
                  SizedBox(
                    height: height / 31,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Button(
                        background: kGrey1,
                        width: width / 3,
                        text: 'Close',
                        onPressed: () {
                          dismiss();
                        },
                      ),
                      Button(
                        background: kPink,
                        width: width / 3,
                        text: 'Add',
                        onPressed: () async {
                          dismiss();
                          context
                              .read<DashboardNotifier>()
                              .addItem(url: urlController.text)
                              .then((value) =>
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        "Add Site item",
                                      ),
                                      duration: Duration(milliseconds: 500),
                                    ),
                                  ));
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void dismiss() {
    Navigator.of(context).pop();
  }
}
