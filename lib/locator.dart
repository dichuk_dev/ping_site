import 'package:get_it/get_it.dart';
import 'package:ping_site/provider/dashboard_notifier.dart';
import 'package:ping_site/ui/screens/dashboard/dashboard.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => DashboardNotifier());

  locator.registerFactory(() => DashboardScreen());
}
