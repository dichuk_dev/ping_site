import 'package:ping_site/utils/routes.dart';

class SplashBloc {
  Future<String> initPage() async {
    return updatePage();
  }

  Future<String> updatePage() async => Routes.dashboard;
}
